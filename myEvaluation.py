#myEvaluation.py
#Klasse zur Auswerung der Sensordaten
from mdbconnection import MDB_CONNECTION
import matplotlib.pyplot as plt
import numpy as np
from datetime import datetime, timedelta
from ipywidgets import interact ,SelectionSlider, FloatSlider
from functools import partial
import pandas as pd

class Evaluation:
    db = None
    def __init__(self):
        self.db = MDB_CONNECTION.getInstance(2) # 2 wählt User Analyzer aus
    def partcallDiffHum(self,sensors,posx,posy):
        plt.figure(figsize=(9.6,7.2)) #figsize=(6, 4.5)
        for s in sensors:
            start=datetime(2024, 5, 31, 20, 36, 0)
            end=datetime(2024, 5, 31, 21, 41, 0)
            time = self.db.getTimes(start,end,s)
            rh = self.db.getHumidity(start,end,s)
            for i in range(len(rh)):
                rh[i] -= 66.5
            if(s>=7):
                plt.plot(time,rh,label=''.join(map(str, self.db.getSensorName(s))),linestyle="dotted")
            else:
                plt.plot(time,rh,label=''.join(map(str, self.db.getSensorName(s))))
        plt.plot([start,end],[0,0],label="korrekt",linestyle="dashed")
        plt.xlabel('Zeit')
        plt.ylabel('Luftfeuchtigkeit')
        plt.legend(loc='upper left', bbox_to_anchor=(posx, posy)) #loc='upper left',bbox_to_anchor=(1,1),fontsize=10,ncol=2
        plt.tight_layout()
        plt.show()
    def partcallAbsolutHum(self,sensors,posx,posy):
        plt.figure(figsize=(9.6,7.2)) #figsize=(6, 4.5)
        for s in sensors:
            start=datetime(2024, 5, 31, 19, 52, 0)
            end=datetime(2024, 5, 31, 20, 19, 0)
            time = self.db.getTimes(start,end,s)
            rh = self.db.getHumidity(start,end,s)
            if(s>=7):
                plt.plot(time,rh,label=''.join(map(str, self.db.getSensorName(s))),linestyle="dotted")
            else:
                plt.plot(time,rh,label=''.join(map(str, self.db.getSensorName(s))))
        plt.plot([start,end],[63,64],label="korrekt",linestyle="dashed")
        plt.xlabel('Zeit')
        plt.ylabel('Luftfeuchtigkeit')
        plt.legend(loc='upper left', bbox_to_anchor=(posx, posy)) #loc='upper left',bbox_to_anchor=(1,1),fontsize=10,ncol=2
        plt.tight_layout()
        plt.show()
    def callAbsolutHum(self,mysensors):
        pos1 = FloatSlider(value=1,min=0,max=2.0,step=0.1,description='X-Anker')
        pos2 = FloatSlider(value=0.9,min=0,max=2.0,step=0.1,description='Y-Anker')
        interact(lambda posx, posy: self.partcallAbsolutHum(mysensors, posx, posy), posx=pos1, posy=pos2)
    def callDiffHum(self,mysensors):
        pos1 = FloatSlider(value=1,min=0,max=2.0,step=0.1,description='X-Anker')
        pos2 = FloatSlider(value=0.9,min=0,max=2.0,step=0.1,description='Y-Anker')
        interact(lambda posx, posy: self.partcallDiffHum(mysensors, posx, posy), posx=pos1, posy=pos2)
    def partialAbsolutHum(self,sensors,start,end,xpos,ypos):
        #print(sensors)
        plt.figure(figsize=(9.6, 7.2))
        for s in sensors:
            time = self.db.getTimes(start,end,s)
            rh = self.db.getHumidity(start,end,s)
            #print(rh)
            if len(time) != len(rh):
                print(f"Data length mismatch for sensor {s}. Skipping this sensor.")
                continue
            if(s>=7):
                plt.plot(time,rh,label=''.join(map(str, self.db.getSensorName(s))),linestyle="dotted")
            else:
                plt.plot(time,rh,label=''.join(map(str, self.db.getSensorName(s))))
        plt.xlabel('Zeit')
        plt.ylabel('Luftfeuchtigkeit')
        plt.legend(loc='upper left', bbox_to_anchor=(xpos, ypos))
        plt.show()
    def partialAbsolutTmp(self,sensors,start,end,xpos,ypos):
        plt.figure(figsize=(9.6,7.2))
        for s in sensors:
            time = self.db.getTimes(start,end,s)
            te = self.db.getTemperature(start,end,s)
            if len(time) != len(te):
                print(f"Data length mismatch for sensor {s}. Skipping this sensor.")
                continue
            if(s>=7):
                plt.plot(time,te,label=''.join(map(str, self.db.getSensorName(s))),linestyle="dotted")
            else:
                plt.plot(time,te,label=''.join(map(str, self.db.getSensorName(s))))
        plt.xlabel('Zeit')
        plt.ylabel('Temperatur')
        plt.legend(loc='upper left', bbox_to_anchor=(xpos, ypos))
        plt.show()
    def absolutDiagramm(self, allsensors, start_point, end_point):
        # Erzeugen Sie eine Sequenz von Datums- und Uhrzeitstempeln im Minutentakt
        datetimes = pd.date_range(start=start_point, end=end_point, freq='min')
        # Konvertieren Sie die pandas-Datumsreihe in eine Liste von datetime Objekten
        datetimes_list = datetimes.to_pydatetime().tolist()
        options = [(i.strftime('%b'), i) for i in datetimes_list]
        start_slider_hum = SelectionSlider(options=options, value=start_point, description='Startzeit')
        end_slider_hum = SelectionSlider(options=options, value=end_point, description='Endzeit')
        start_slider_tmp = SelectionSlider(options=options, value=start_point, description='Startzeit')
        end_slider_tmp = SelectionSlider(options=options, value=end_point, description='Endzeit')
        pos1 = FloatSlider(value=1,min=0,max=2.0,step=0.1,description='X-Anker')
        pos2 = FloatSlider(value=0.9,min=0,max=2.0,step=0.1,description='Y-Anker')
        # Use partial to bind the static parameters
        partial_func_hum = partial(self.partialAbsolutHum)
        partial_func_hum.__name__ = self.partialAbsolutHum.__name__
        # Use interact to dynamically update the plot
        interact(lambda start, end,xpos,ypos: partial_func_hum(allsensors, start, end,xpos,ypos), start=start_slider_hum, end=end_slider_hum,xpos=pos1,ypos=pos2)
        # Use partial to bind the static parameters
        partial_func_tmp = partial(self.partialAbsolutTmp)
        partial_func_tmp.__name__ = self.partialAbsolutTmp.__name__
        # Use interact to dynamically update the plot
        interact(lambda start, end,xpos,ypos: partial_func_tmp(allsensors, start, end,xpos,ypos), start=start_slider_tmp, end=end_slider_tmp,xpos=pos1,ypos=pos2) 
    def partialDiffTmp(self,allsensors,base,start,end,xpos,ypos):
        plt.clf()
        baseTmp = self.db.getTemperature(start,end,base)
        baseTime = self.db.getTimes(start,end,base)
        for s in allsensors:
            if s != base:
                difftemps = np.array([])
                sTmps = np.array([])
                sTimes = np.array([])
                sTmps = self.db.getHumidity(start,end,s)
                sTimes = self.db.getTimes(start,end,s)
                deletionlist=[]
                for i,time1 in enumerate(sTimes):
                    check=0
                    for j,time4 in enumerate(baseTime):
                        if((time1-timedelta(seconds=10)) <= time4 <= (time1+timedelta(seconds=10))):
                            difftemps = np.append(difftemps,sTmps[i]-baseTmp[j])
                            check=1
                            break
                    if(check==0):
                        deletionlist.append(i)
                length = len(deletionlist)
                i=0
                while i < length:
                    sTimes=np.delete(sTimes,int(deletionlist[i]-i))
                    i=i+1
                if  (np.size(sTimes)>0):
                    #print(s)
                    if(s>=7):
                        plt.plot(sTimes,difftemps,label=''.join(map(str, self.db.getSensorName(s))),linestyle="dotted")
                    else:
                        plt.plot(sTimes,difftemps,label=''.join(map(str, self.db.getSensorName(s))))
        plt.xlabel('Zeit')
        plt.ylabel('Temperatur')
        plt.legend(loc='upper left', bbox_to_anchor=(xpos, ypos))
        plt.show()
    def partialDiffHum(self,allsensors,base,start,end,xpos,ypos):
        #print(allsensors)
        plt.clf()
        baseHum = self.db.getHumidity(start,end,base)
        baseTime = self.db.getTimes(start,end,base)
        for s in allsensors:
            if s != base:
                diffhums = np.array([])
                sHums = np.array([])
                sTimes = np.array([])
                sHums = self.db.getHumidity(start,end,s)
                sTimes = self.db.getTimes(start,end,s)
                deletionlist=[]
                for i,time1 in enumerate(sTimes):
                    check=0
                    for j,time4 in enumerate(baseTime):
                        if((time1-timedelta(seconds=10)) <= time4 <= (time1+timedelta(seconds=10))):
                            diffhums = np.append(diffhums,sHums[i]-baseHum[j])
                            check=1
                            break
                    if(check==0):
                        deletionlist.append(i)
                length = len(deletionlist)
                #print(length)
                i=0
                while i < length:
                    sTimes=np.delete(sTimes,int(deletionlist[i]-i))
                    i=i+1
                if  (np.size(sTimes)>0):
                    if(s>=7):
                        plt.plot(sTimes,diffhums,label=''.join(map(str, self.db.getSensorName(s))),linestyle="dotted")
                    else:
                        plt.plot(sTimes,diffhums,label=''.join(map(str, self.db.getSensorName(s))))
        plt.xlabel('Zeit')
        plt.ylabel('Luftfeuchtigkeit')
        plt.legend(loc='upper left', bbox_to_anchor=(xpos, ypos))
        plt.show()
    def diffDiagramm(self, basesensor,sensors, start_point, end_point):
        # Erzeugen Sie eine Sequenz von Datums- und Uhrzeitstempeln im Minutentakt
        datetimes = pd.date_range(start=start_point, end=end_point, freq='min')
        # Konvertieren Sie die pandas-Datumsreihe in eine Liste von datetime Objekten
        datetimes_list = datetimes.to_pydatetime().tolist()
        options = [(i.strftime('%b'), i) for i in datetimes_list]
        start_slider_tmp = SelectionSlider(options=options, value=start_point, description='Startzeit')
        end_slider_tmp = SelectionSlider(options=options, value=end_point, description='Endzeit')
        start_slider_hum = SelectionSlider(options=options, value=start_point, description='Startzeit')
        end_slider_hum = SelectionSlider(options=options, value=end_point, description='Endzeit')
        pos1 = FloatSlider(value=0.8,min=0,max=2.0,step=0.1,description='X-Anker')
        pos2 = FloatSlider(value=0.9,min=0,max=2.0,step=0.1,description='Y-Anker')
        #plot_hum_partial = partial(self.partialDiffHum, self,sensors,basesensor)
        #plot_hum_partial.__name__ = self.partialDiffHum.__name__
        # Use partial to bind the static parameters
        partial_func_hum = partial(self.partialDiffHum)
        partial_func_hum.__name__ = self.partialDiffHum.__name__
        # Use interact to dynamically update the plot
        interact(lambda start, end,xpos,ypos: partial_func_hum(sensors,basesensor, start, end,xpos,ypos), start=start_slider_hum, end=end_slider_hum,xpos=pos1,ypos=pos2)
        #partial_func_tmp = partial(self.partialDiffTmp)
        #partial_func_tmp.__name__ = self.partialDiffTmp.__name__
        # Use interact to dynamically update the plot
        #interact(lambda start, end,xpos,ypos: partial_func_tmp(sensors,basesensor, start, end,xpos,ypos), start=start_slider_tmp, end=end_slider_tmp,xpos=pos1,ypos=pos2)
    def corHumidity(self,sensors,start,end):
        plt.figure(figsize=(9.6, 7.2))
        cor = [-10.6231,-11.459,0,0.1648,-9.37,-9.786,-5.54887,-3.3325,-3.3577,-2.8926,19.67,8.19,5.07,9.57,-2.5356,-3.73]
        for s in sensors:
            time = self.db.getTimes(start,end,s)
            rh = np.array(self.db.getHumidity(start,end,s))
            rh -= cor[s-1]
            #print(rh)
            if len(time) != len(rh):
                print(f"Data length mismatch for sensor {s}. Skipping this sensor.")
                continue
            if(s>=7):
                plt.plot(time,rh,label=''.join(map(str, self.db.getSensorName(s))),linestyle="dotted")
            else:
                plt.plot(time,rh,label=''.join(map(str, self.db.getSensorName(s))))
        plt.xlabel('Zeit')
        plt.ylabel('Luftfeuchtigkeit')
        plt.legend(loc='upper left', bbox_to_anchor=(0.8, 0.9))
        plt.show()
    def medianDiff(self,sensors,start,end):
        baseTimes = np.array(self.db.getTimes(start,end,3))
        for s in sensors:
            baseHum = np.array(self.db.getHumidity(start,end,3))
            timeOne = np.array(self.db.getTimes(start,end,s))
            humiOne = np.array(self.db.getHumidity(start,end,s))
            if(baseTimes.size == timeOne.size):
                print("Arrays sind gleich gross")
            else:
                deletionlist=[]
                for i,time1 in enumerate(baseTimes):
                    check=0
                    for j,time4 in enumerate(timeOne):
                        if((time1-timedelta(seconds=10)) <= time4 <= (time1+timedelta(seconds=10))):
                            check=1
                            break
                    if(check==0):
                        deletionlist.append(i)
                length = len(deletionlist)
                print(deletionlist)
                i=0
                while i < length:
                    if((baseHum is None) or (baseHum.size==0)):
                        break
                    baseHum=np.delete(baseHum,int(deletionlist[i]-i))
                    i=i+1
            if((baseHum is None) or (baseHum.size==0)):
                print("leer")
            else:
                diffs = humiOne - baseHum 
            print (np.median(diffs))