"Klasse für Datenbankverbindung"
"nach Signleton Vorlage von: https://www.tutorialspoint.com/python_design_patterns/python_design_patterns_singleton.htm"

import os
import sys
import mariadb
from configparser import ConfigParser
from datetime import datetime, timedelta

class MDB_CONNECTION:
   __instance = None #Speichert die Instanz für Singleton
   conn = None #Speichert die Datenbankverbindung
   @staticmethod 
   def getInstance(user): #Kontrolliert ob bereits eine Instanz existiert und ruft sonst den Konstruktor auf
      """ Statische Zugriffsmethode. """
      if MDB_CONNECTION.__instance == None:
         MDB_CONNECTION(user)
      return MDB_CONNECTION.__instance
   def __init__(self,user): #Initialisiert die Datenbankverbindung
      """ Birtueller privater Konstruktor """
      if MDB_CONNECTION.__instance != None:
         raise Exception("This class is a Singleton you can't create more Instances!")
      else:
         MDB_CONNECTION.__instance = self
         # Quelle OS: https://www.geeksforgeeks.org/python-os-getcwd-method/
         working_directory = os.getcwd() #Location des current working directory
         file_path = working_directory + "/config.ini" #Location des config files
         #Auslesen der Datei
         config = ConfigParser
         config = ConfigParser()
         print(file_path)
         print (config.read(file_path))
         DB_HOST  = config.get("database","DB_HOST")
         DB_NAME  = config.get("database","DB_NAME")
         DB_PORT  = config.getint("database","DB_PORT")
         if(user==1):
            DB_USER  = config.get("database","DB_USER_SENSOR")
            DB_PW    = config.get("database","DB_PW_SENSOR")
         if(user==2):
            DB_USER  = config.get("database","DB_USER_ANALYZER")
            DB_PW    = config.get("database","DB_PW_ANALYZER")
         try:
            self.conn = mariadb.connect(
               user     = DB_USER,
               password = DB_PW,
               host     = DB_HOST,
               port     = DB_PORT,
               database = DB_NAME
               )
         except mariadb.Error as e:
            print(f"Error connecting to MariaDB Platform: {e}")
            sys.exit(1)

   def insertValue(self,temp,humidity,sensor):
      """Fügt eine Messung in die Tabelle einm"""
      """Parameter: eigene Instanz der Klasse, Temperatur, relative Luftfeuchtigkeit, SensorID"""
      """Rückgabeparameter: String Reaction - Reaktion auf letztes Klingeln"""
      cur = self.conn.cursor()
      query = f"INSERT INTO SensorVals (Temperature, Humidity, Sensor_ID) VALUES ({temp},{humidity},{sensor})"
      cur.execute(query)
      self.conn.commit()
      return
   def getMaxTime(self,start,end):
      """Liefert die späteste Zeit für die es einen Eintrag gibt"""
      """Parameter: eigene Instanz der Klasse, Startzeitpunkt, Endzeitpunkt"""
      """Rückgabeparameter: datetime Time - der späteste existierende Zeitpunkt"""
      cur = self.conn.cursor()
      query = "SELECT Max(CurrentDateTime) FROM SensorVals WHERE CurrentDateTime >= %s AND CurrentDateTime <= %s"
      cur.execute(query, (start, end))
      #query=f"SELECT Max(CurrentDateTime) FROM SensorVals WHERE  CurrentDateTime >={start} AND CurrentDateTime <={end}"
      #cur.execute(query)
      result = cur.fetchone() # Holt sich die erste Zeile
      if result:
         return result[0] #Funtkioniert weil es nur einen Eintrag in dieser Tabelle gibt
      return None # None Rückgabe falls kein Ergebnis
   def getMinTime(self,start,end):
      """Liefert die frühesteste Zeit für die es einen Eintrag gibt"""
      """Parameter: eigene Instanz der Klasse, Startzeitpunkt, Endzeitpunkt"""
      """Rückgabeparameter: datetime Time - der früheste existierende Zeitpunkt"""
      cur = self.conn.cursor()
      query="SELECT Min(CurrentDateTime) FROM SensorVals WHERE CurrentDateTime >= %s AND CurrentDateTime <= %s"
      cur.execute(query, (start, end))
      result = cur.fetchone() # Holt sich die erste Zeile
      if result:
         return result[0] #Funtkioniert weil es nur einen Eintrag in dieser Tabelle gibt
      return None # None Rückgabe falls kein Ergebnis
   def getMinTmp(self,start,end):
      """Liefert die niedrigste Temperatur"""
      """Parameter: eigene Instanz der Klasse, Startzeitpunkt, Endzeitpunkt"""
      """Rückgabeparameter: float Temperature - der niedrigste Temperatur"""
      cur = self.conn.cursor()
      query="SELECT Min(Temperature) FROM SensorVals WHERE CurrentDateTime >= %s AND CurrentDateTime <= %s"
      cur.execute(query, (start, end))
      result = cur.fetchone() # Holt sich die erste Zeile
      if result:
         return result[0] #Funtkioniert weil es nur einen Eintrag in dieser Tabelle gibt
      return None # None Rückgabe falls kein Ergebnis
   def getMaxTmp(self,start,end):
      """Liefert die höchste Temperatur für die es einen Eintrag gibt"""
      """Parameter: eigene Instanz der Klasse, Startzeitpunkt, Endzeitpunkt"""
      """Rückgabeparameter: float Temperature - der höchste Temperatur"""
      cur = self.conn.cursor()
      query="SELECT Min(Temperature) FROM SensorVals WHERE  AND CurrentDateTime >= %s AND CurrentDateTime <= %s"
      cur.execute(query, (start, end))
      result = cur.fetchone() # Holt sich die erste Zeile
      if result:
         return result[0] #Funtkioniert weil es nur einen Eintrag in dieser Tabelle gibt
      return None # None Rückgabe falls kein Ergebnis
   def getMinHum(self,start,end):    
      """Liefert die niedrigste Luftfeuchtigkeit"""
      """Parameter: eigene Instanz der Klasse, Startzeitpunkt, Endzeitpunkt"""
      """Rückgabeparameter: float Humidity - der niedrigste Luftfeuchtigkeit"""
      cur = self.conn.cursor()
      query="SELECT Min(Humidity) FROM SensorVals WHERE CurrentDateTime >= %s AND CurrentDateTime <= %s"
      cur.execute(query, (start, end))
      result = cur.fetchone() # Holt sich die erste Zeile
      if result:
         return result[0] #Funtkioniert weil es nur einen Eintrag in dieser Tabelle gibt
      return None # None Rückgabe falls kein Ergebnis
   def getMaxHum(self,start,end):
      """Liefert die höchste Luftfeuchtigkeit"""
      """Parameter: eigene Instanz der Klasse, Startzeitpunkt, Endzeitpunkt"""
      """Rückgabeparameter: float Humidity - der höchste Luftfeuchtigkeit"""
      cur = self.conn.cursor()
      query="SELECT Max(Humidity) FROM SensorVals WHERE CurrentDateTime >=%s AND CurrentDateTime <=%s"
      cur.execute(query, (start, end))
      result = cur.fetchone() # Holt sich die erste Zeile
      if result:
         return result[0] #Funtkioniert weil es nur einen Eintrag in dieser Tabelle gibt
      return None # None Rückgabe falls kein Ergebnis
   def getTimes(self,start,end,sensor):
      """Liefert die Zeiten für einen Sensor"""
      """Parameter: eigene Instanz der Klasse, Startzeitpunkt, Endzeitpunkt, Sensor"""
      """Rückgabeparameter: datetime[] times - Array mit den Messzeitpunkten"""
      cur = self.conn.cursor()
      query=f"SELECT CurrentDateTime FROM SensorVals WHERE CurrentDateTime >='{start}' AND CurrentDateTime <='{end}' AND Sensor_ID={sensor}"
      cur.execute(query)
      times = cur.fetchall() 
      ReturnArray = []
      for CurrentDateTime in  times :
         ReturnEntry = CurrentDateTime[0]
         ReturnArray.append(ReturnEntry)
      return ReturnArray
   def getHumidity(self,start,end,sensor):
      """Liefert die Luftfeuchtigkeitswerte für einen Sensor"""
      """Parameter: eigene Instanz der Klasse, Startzeitpunkt, Endzeitpunkt, Sensor"""
      """Rückgabeparameter: float[] humidity - Array mit den Luftfeuchtigkeitswerten"""
      cur = self.conn.cursor()
      query=f"SELECT Humidity FROM SensorVals WHERE CurrentDateTime >='{start}' AND CurrentDateTime <='{end}' AND Sensor_ID={sensor}"
      cur.execute(query)
      myhumidity = cur.fetchall() 
      ReturnArray = []
      for Humidity in  myhumidity :
         ReturnEntry = Humidity[0]
         ReturnArray.append(ReturnEntry)
      return ReturnArray
   def getDiffHum(self,start,end,sensor,base):
      ReturnArray = []
      s = start
      e = start+timedelta(seconds=20)
      while(e<end):
         cur = self.conn.cursor()
         query=f"SELECT Min(Humidity) FROM SensorVals WHERE CurrentDateTime >='{s}' AND CurrentDateTime <='{e}' AND Sensor_ID={sensor}"
         cur.execute(query)
         hum_s = cur.fetchone() 
         query=f"SELECT Min(Humidity) FROM SensorVals WHERE CurrentDateTime >='{s}' AND CurrentDateTime <='{e}' AND Sensor_ID={base}"
         cur.execute(query)
         hum_b = cur.fetchone() 
         if( (hum_b[0] is not None) and (hum_s[0] is  not None)):
            ReturnArray.append(hum_s[0]-hum_b[0])
         else:
            ReturnArray.append(0)
         s += timedelta(minutes=1)
         e += timedelta(minutes=1)
      return ReturnArray
   def getTemperature(self,start,end,sensor):
      """Liefert die Temperaturwerte für einen Sensor"""
      """Parameter: eigene Instanz der Klasse, Startzeitpunkt, Endzeitpunkt, Sensor"""
      """Rückgabeparameter: float[] tmp - Array mit den Temperaturwerten"""
      cur = self.conn.cursor()
      query=f"SELECT Temperature FROM SensorVals WHERE CurrentDateTime >='{start}' AND CurrentDateTime <='{end}' AND Sensor_ID={sensor}"
      cur.execute(query)
      tmp = cur.fetchall() 
      ReturnArray = []
      for Temperature in  tmp :
         ReturnEntry = Temperature[0]
         ReturnArray.append(ReturnEntry)
      return ReturnArray
   def getDiffTmp(self,start,end,sensor,base):
      ReturnArray = []
      s = start
      e = start+timedelta(seconds=20)
      while(e<end):
         cur = self.conn.cursor()
         query=f"SELECT Min(Temperature)) FROM SensorVals WHERE CurrentDateTime >='{s}' AND CurrentDateTime <='{e}' AND Sensor_ID={sensor}"
         cur.execute(query)
         tmp_s = cur.fetchone() 
         query=f"SELECT Min(Temperature) FROM SensorVals WHERE CurrentDateTime >='{s}' AND CurrentDateTime <='{e}' AND Sensor_ID={base}"
         cur.execute(query)
         tmp_b = cur.fetchone() 
         if((tmp_b[0] is not None) and (tmp_s[0] is not None)):
            ReturnArray.append(tmp_s[0]-tmp_b[0])
         else:
            ReturnArray.append(0)
         s += timedelta(minutes=1)
         e += timedelta(minutes=1)
      return ReturnArray
   def getSensorName(self,id):
      """Liefert den Namen des Sensors"""
      """Parameter: eigene Instanz der Klasse, id"""
      """Rückgabeparameter: string name - Name des Sensors"""
      cur = self.conn.cursor()
      query=f"SELECT SensorType, SensorColor FROM Sensor WHERE SensorID ={id}"
      cur.execute(query) 
      labelparts = cur.fetchall() 
      for (SensorType,SensorColor) in  labelparts :
         name = f"{SensorType} {SensorColor}"
         return name
      return None # Bei Fehler return None
   def getSensorID(self,type,color):
      """Liefert die ID des Sensors je nach Typ und Farbe"""
      """Parameter: eigene Instanz der Klasse, Typ, Farbe"""
      """Rückgabeparameter: int ID - die ID des ausgewählten Sensors"""
      cur = self.conn.cursor()
      query=f"SELECT SensorID FROM Sensor WHERE SensorType='{type}' AND SensorColor='{color}'"
      cur.execute(query)
      result = cur.fetchone() # Holt sich die erste Zeile
      if result:
         return result[0] #Funtkioniert weil es nur einen Eintrag in dieser Tabelle gibt
      return None # None Rückgabe falls kein Ergebnis