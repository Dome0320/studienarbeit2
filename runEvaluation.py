#%matplotlib widget

from myEvaluation import Evaluation
from datetime import date, time, datetime

eval = Evaluation()
sensors = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]
#choice = input("1:Grafik 2:kGrafik 3:OneSensor")
# Benutzereingabe abfragen
#YYYY-MM-DD HH:MM:SS
start_day = input("Geben Sie den Starttag ein: ")
end_day = input("Geben Sie den Endtag ein: ")
base = input("Geben Sie die ID des Base Sensor ein: ")
try:
    # Versuche, die Benutzereingabe in ein datetime-Objekt zu konvertieren
    start = datetime(2024, 5, int(start_day), 0, 0, 0) #?
    end = datetime(2024, 6, int(end_day), 0, 0, 0)
except ValueError:
    print("Ungültiges Datums- und Uhrzeitformat. Bitte verwenden Sie das Format YYYY-MM-DD HH:MM:SS.")
eval.callAbsolutHum(sensors)
eval.callDiffHum(sensors)
eval.absolutDiagramm(sensors,start,end)
#eval.diffDiagramm(base,sensors,start,end)
eval.medianDiff(sensors,start,end)