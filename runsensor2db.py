# kleines Programm um die Erreichbarkeit der Sensoren zu testen
# nutzt die Gleichen Bibliotheken wie das Programm zur Datenaufnahme

#HTU21D nach https://github.com/ncdcommunity/Raspberry_Pi_HTU21D_Humidity_Temperature_Sensor_Python_library/blob/master/HTU21D.py
#Quelle der AHT library https://github.com/etno712/aht

import RPi.GPIO as GPIO
import time
import board
from board import *
from adafruit_bme280 import basic as adafruit_bme280
import adafruit_sht31d
import adafruit_am2320
import adafruit_ahtx0
from adafruit_htu21d import HTU21D
import dht22 # 2 oder 
import busio
import adafruit_bitbangio
from DFRobot_DHT20 import DFRobot_DHT20
from mdbconnection import MDB_CONNECTION
import smbus2
import bme280


#Datenbank Connection
db = MDB_CONNECTION.getInstance(1) #1 wählt DB Connection mit user Sensor aus

port_bme_o = 4
port_bme_p = 5
i2c_sda = board.D23
i2c_scl = board.D24
bme_address= 0x76 # I2C Adresse des ersten BME280 Sensors
sht_address= 0x44
htu_address= 0x40
aht_address= 0x38
# I2C4 Bus
bus_bme_o=smbus2.SMBus(port_bme_o)
calibration_params_o = bme280.load_calibration_params(bus_bme_o, bme_address)
# I2C1 Bus
i2c_g = board.I2C() # nutzt den Standrard i2C Bus
sensor_BME_g = adafruit_bme280.Adafruit_BME280_I2C(i2c_g,bme_address)
sensor_SHT_g = adafruit_sht31d.SHT31D(i2c_g)
sensor_HTU_g = HTU21D(i2c_g)
sensor_AHT_g = adafruit_ahtx0.AHTx0(i2c_g)
sensor_DHT_4 = DFRobot_DHT20(0x04,0x38)
# I2C5 Bus
bus_bme_p=smbus2.SMBus(port_bme_p)
calibration_params_p = bme280.load_calibration_params(bus_bme_p, bme_address)
# I2C3 Bus
i2c_b = adafruit_bitbangio.I2C(scl=i2c_scl, sda=i2c_sda)
sensor_BME_b = adafruit_bme280.Adafruit_BME280_I2C(i2c_b,bme_address)
sensor_SHT_b = adafruit_sht31d.SHT31D(i2c_b)
sensor_HTU_b = HTU21D(i2c_b)
sensor_AHT_b = adafruit_ahtx0.AHTx0(i2c_b)
sensor_DHT_5 = DFRobot_DHT20(0x05,0x38)
#folgender Code ist von https://pypi.org/project/dht11/ <-- brauchts das DHT22??
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
sensor_22_b = dht22.DHT22(pin = 17)
sensor_22_g = dht22.DHT22(pin = 27)
sensor_22_p = dht22.DHT22(pin = 22)
sensor_22_o = dht22.DHT22(pin = 25)
start = time.perf_counter()
while(1):
    if(time.perf_counter()>start):
        #BME280
        print("BME280 gruen")
        id = db.getSensorID("BME280",'g')
        print(id)
        db.insertValue(sensor_BME_g.temperature,sensor_BME_g.relative_humidity,id)
        print("BME280 blau")
        id = db.getSensorID("BME280",'b')
        print(id)
        db.insertValue(sensor_BME_b.temperature,sensor_BME_b.relative_humidity,id)
        print("BME280 orange")
        id = db.getSensorID("BME280",'o')
        print(id)
        data = bme280.sample(bus_bme_o, bme_address, calibration_params_o)
        print("Temperatur: %0.1f %%" % data.temperature)
        print("Humidity: %0.1f %%" % data.humidity)
        db.insertValue(data.temperature,data.humidity,id)
        print("BME280 pink")
        id = db.getSensorID("BME280",'p')
        print(id)
        data = bme280.sample(bus_bme_p, bme_address, calibration_params_p)
        print("Temperatur: %0.1f %%" % data.temperature)
        print("Humidity: %0.1f %%" % data.humidity)
        db.insertValue(data.temperature,data.humidity,id)
        #SHT31
        print("SHT31 gruen")
        id = db.getSensorID("SHT31",'g')
        print(id)
        db.insertValue(sensor_SHT_g.temperature,sensor_SHT_g.relative_humidity,id)
        print("SHT31 blau")
        id = db.getSensorID("SHT31",'b')
        print(id)
        db.insertValue(sensor_SHT_b.temperature,sensor_SHT_b.relative_humidity,id)
        #HTU21D
        try:
            print("HTU21D gruen")
            id = db.getSensorID("HTU21D",'g')
            print(id)
            db.insertValue(sensor_HTU_g.temperature,sensor_HTU_g.relative_humidity,id)
        except IOError as e:
            print(f"Failed to read bytes from HTU21D: {e}") 
        print("HTU21D blau")
        id = db.getSensorID("HTU21D",'b')
        print(id)
        db.insertValue(sensor_HTU_b.temperature,sensor_HTU_b.relative_humidity,id)
        #AHT25
        print("AHT25 gruen")
        id = db.getSensorID("AHT25",'g')
        print(id)
        db.insertValue(sensor_AHT_g.temperature,sensor_AHT_g.relative_humidity,id)
        print("AHT25 blau")
        id = db.getSensorID("AHT25",'b')
        print(id)
        db.insertValue(sensor_AHT_b.temperature,sensor_AHT_b.relative_humidity,id)
        # DHT20
        T_celcius, humidity, crc_error = sensor_DHT_4.get_temperature_and_humidity()
        print("DHT20 gruen")
        id = db.getSensorID("DHT20",'g')
        print(id)
        db.insertValue(T_celcius,humidity,id)
        T_celcius, humidity, crc_error = sensor_DHT_5.get_temperature_and_humidity()
        print("DHT20 blau")
        id = db.getSensorID("DHT20",'b')
        print(id)
        db.insertValue(T_celcius,humidity,id)
        time.sleep(2)
        #DHT22
        dht22_b_result = sensor_22_b.read()
        if dht22_b_result.is_valid():
            print("DHT22 Blau")
            id = db.getSensorID("DHT22",'b')
            print(id)
            db.insertValue(dht22_b_result.temperature,dht22_b_result.humidity,id)
        else:
            print("Error: %d" % dht22_b_result.error_code)    
        dht22_g_result = sensor_22_g.read()
        if dht22_g_result.is_valid():
            print("DHT22 Gruen")
            id = db.getSensorID("DHT22",'g')
            print(id)
            db.insertValue(dht22_g_result.temperature,dht22_g_result.humidity,id)
        else:
            print("Error: %d" % dht22_g_result.error_code)
        dht22_p_result = sensor_22_p.read()
        if dht22_p_result.is_valid():
            print("DHT22 Pink (neu)")
            id = db.getSensorID("DHT22",'p')
            print(id)
            db.insertValue(dht22_p_result.temperature,dht22_p_result.humidity,id)
        else:
            print("Error: %d" % dht22_p_result.error_code)
        dht22_o_result = sensor_22_o.read()
        if dht22_o_result.is_valid():
            print("DHT22 Orange (neu)")
            id = db.getSensorID("DHT22",'o')
            print(id)
            db.insertValue(dht22_o_result.temperature,dht22_o_result.humidity,id)
        else:
            print("Error: %d" % dht22_o_result.error_code)    
        start=time.perf_counter()+60