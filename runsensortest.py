# kleines Programm um die Erreichbarkeit der Sensoren zu testen
# nutzt die Gleichen Bibliotheken wie das Programm zur Datenaufnahme

#HTU21D nach https://github.com/ncdcommunity/Raspberry_Pi_HTU21D_Humidity_Temperature_Sensor_Python_library/blob/master/HTU21D.py
#Quelle der AHT library https://github.com/etno712/aht

import RPi.GPIO as GPIO
import time
import board
from board import *
from adafruit_bme280 import basic as adafruit_bme280
import adafruit_sht31d
import adafruit_am2320
import adafruit_ahtx0
from adafruit_htu21d import HTU21D
import dht22 # 2 oder 
import busio
import adafruit_bitbangio
from DFRobot_DHT20 import DFRobot_DHT20
import smbus2
import bme280

port_bme_o = 4
port_bme_p = 5
i2c_sda = board.D23
i2c_scl = board.D24
#i2c_sda_4 = board.D5
#i2c_scl_4 = board.D6
bme_address= 0x76 # I2C Adresse des ersten BME280 Sensors
sht_address= 0x44
htu_address= 0x40
aht_address= 0x38
# I2C1 Bus
i2c_g = board.I2C() # nutzt den Standrard i2C Bus
#try:
#    i2c_g_4 = adafruit_bitbangio.I2C(scl=i2c_scl_4, sda=i2c_sda_4) #Bus 4 da AM2320 am Bus nicht angezeigt wurde
#except RuntimeError as e:
#     print(f"Failed AM2320 grün Bus init: {e}") 

bus_bme_o=smbus2.SMBus(port_bme_o)
calibration_params_o = bme280.load_calibration_params(bus_bme_o, bme_address)
sensor_BME_g = adafruit_bme280.Adafruit_BME280_I2C(i2c_g,bme_address)
sensor_SHT_g = adafruit_sht31d.SHT31D(i2c_g)
sensor_HTU_g = HTU21D(i2c_g)
sensor_AHT_g = adafruit_ahtx0.AHTx0(i2c_g)
#try:
#    sensor_AM_g = adafruit_am2320.AM2320(i2c_g_4)
#except RuntimeError as e:
#    print(f"Failed AM2320 gruen init: {e}") 
sensor_DHT_4 = DFRobot_DHT20(0x04,0x38)
#bus_g_aht =  machine.I2C(scl=machine.Pin(3), sda=machine.Pin(2))
#aht_g = aht.AHT2x(bus_g, crc=True)
# I2C3 Bus
i2c_b = adafruit_bitbangio.I2C(scl=i2c_scl, sda=i2c_sda)
bus_bme_p=smbus2.SMBus(port_bme_p)
calibration_params_p = bme280.load_calibration_params(bus_bme_p, bme_address)
sensor_BME_b = adafruit_bme280.Adafruit_BME280_I2C(i2c_b,bme_address)
sensor_SHT_b = adafruit_sht31d.SHT31D(i2c_b)
sensor_HTU_b = HTU21D(i2c_b)
sensor_AHT_b = adafruit_ahtx0.AHTx0(i2c_b)
sensor_DHT_5 = DFRobot_DHT20(0x05,0x38)
#try:
#    sensor_AM_b = adafruit_am2320.AM2320(i2c_b)
#except RuntimeError as e:
#    print(f"Failed AM2320 blau init: {e}") 

# calibration_params = bme280.load_calibration_params(bus_b, bme_address)

#bus_b_aht =  machine.I2C(scl=machine.Pin(24), sda=machine.Pin(23))
#aht_b = aht.AHT2x(bus_b, crc=True)
#folgender Code ist von https://pypi.org/project/dht11/ <-- brauchts das DHT22??
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
sensor_22_b = dht22.DHT22(pin = 17)
sensor_22_g = dht22.DHT22(pin = 27)
sensor_22_p = dht22.DHT22(pin = 22)
sensor_22_o = dht22.DHT22(pin = 25)
start = time.perf_counter()
while(1):
    if(time.perf_counter()>start):
        #BME280
        print("BME280 gruen")
        print("\nTemperature: %0.1f C" % sensor_BME_g.temperature)
        print("Humidity: %0.1f %%" % sensor_BME_g.relative_humidity)
        print("BME280 blau")
        print("\nTemperature: %0.1f C" % sensor_BME_b.temperature)
        print("Humidity: %0.1f %%" % sensor_BME_b.relative_humidity)
        print("BME280 orange")
        data = bme280.sample(bus_bme_o, bme_address, calibration_params_o)
        print("Temperatur: %0.1f %%" % data.temperature)
        print("Humidity: %0.1f %%" % data.humidity)
        print("BME280 pink")
        data = bme280.sample(bus_bme_p, bme_address, calibration_params_p)
        print("Temperatur: %0.1f %%" % data.temperature)
        print("Humidity: %0.1f %%" % data.humidity)
        #SHT31
        print("SHT31 gruen")
        print("Temperatur: %0.1f %% C" % sensor_SHT_g.temperature)
        print("Humidity: %0.1f %% RH" % sensor_SHT_g.relative_humidity)
        print("SHT31 blau")
        print("Temperatur: %0.1f %% C" % sensor_SHT_g.temperature)
        print("Humidity: %0.1f %% RH" % sensor_SHT_g.relative_humidity)
        #HTU21D
        try:
            print("HTU21D gruen")
            print("\nTemperature: %0.1f C" % sensor_HTU_g.temperature)
            print("Humidity: %0.1f %%" % sensor_HTU_g.relative_humidity)
        except IOError as e:
            print(f"Failed to read bytes from HTU21D: {e}") 
        print("HTU21D blau")
        print("\nTemperature: %0.1f C" % sensor_HTU_b.temperature)
        print("Humidity: %0.1f %%" % sensor_HTU_b.relative_humidity)
        #AHT25
        print("AHT25 gruen")
        print("\nTemperature: %0.1f C" % sensor_AHT_g.temperature)
        print("Humidity: %0.1f %%" % sensor_AHT_g.relative_humidity)
        time.sleep(2)
        print("AHT25 blau")
        print("\nTemperature: %0.1f C" % sensor_AHT_b.temperature)
        print("Humidity: %0.1f %%" % sensor_AHT_b.relative_humidity)
        time.sleep(2)
        #AM2320
        #try:
        #    print("AM2320 gruen")
        #    print("Temperature: ", sensor_AM_g.temperature)
        #    print("Humidity: ", sensor_AM_g.relative_humidity)
        #except RuntimeError as e:
        #    print(f"Failed AM2320 gruen lesen: {e}") 
        #time.sleep(2)
        #try:
        #    print("AM2320 blau")
        #    print("Temperature: ", sensor_AM_b.temperature)
        #    print("Humidity: ", sensor_AM_b.relative_humidity)
        #except RuntimeError as e:
        #    print(f"Failed AM2320 blau lesen: {e}") 
        #except NameError as e:
        #    print(f"Failed AM2320 blau lesen: {e}") 
        # DHT20
        T_celcius, humidity, crc_error = sensor_DHT_4.get_temperature_and_humidity()
        print("DHT20 gruen")
        print("CRC Fehler        : Error ",crc_error)
        print("Temperature       : %f\u00b0C " %T_celcius)
        print("Relative Humidity : %f %%" %humidity)
        T_celcius, humidity, crc_error = sensor_DHT_5.get_temperature_and_humidity()
        print("DHT20 blau")
        print("CRC Fehler        : Error ",crc_error)
        print("Temperature       : %f\u00b0C " %T_celcius)
        print("Relative Humidity : %f %%" %humidity)
        time.sleep(2)
        #DHT22
        dht22_b_result = sensor_22_b.read()
        if dht22_b_result.is_valid():
            print("DHT22 Blau")
            print("Temperature: %-3.1f C" % dht22_b_result.temperature)
            print("Humidity: %-3.1f %%" % dht22_b_result.humidity)
        else:
            print("Error: %d" % dht22_b_result.error_code)    
        dht22_g_result = sensor_22_g.read()
        if dht22_g_result.is_valid():
            print("DHT22 Gruen")
            print("Temperature: %-3.1f C" % dht22_g_result.temperature)
            print("Humidity: %-3.1f %%" % dht22_g_result.humidity)
        else:
            print("Error: %d" % dht22_g_result.error_code)
        dht22_p_result = sensor_22_p.read()
        if dht22_p_result.is_valid():
            print("DHT22 Pink (neu)")
            print("Temperature: %-3.1f C" % dht22_p_result.temperature)
            print("Humidity: %-3.1f %%" % dht22_p_result.humidity)
        else:
            print("Error: %d" % dht22_p_result.error_code)
        dht22_o_result = sensor_22_o.read()
        if dht22_o_result.is_valid():
            print("DHT22 Orange (neu)")
            print("Temperature: %-3.1f C" % dht22_o_result.temperature)
            print("Humidity: %-3.1f %%" % dht22_o_result.humidity)
        else:
            print("Error: %d" % dht22_o_result.error_code)    

        start=time.perf_counter()+60
#GPIO.cleanup()