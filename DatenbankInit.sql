--basiert auf databes-commands.sql von Stefan Stumpf
--DROP DATABASE test;
--CREATE DATABASE test;
--USE DATABASE test;
--CREATE TABLE tabelle (ID INT NOT NULL AUTO_INCREMENT, TAG DATE DEFAULT NOW(), ZEIT TIME DEFAULT NOW(), PRIMARY KEY (ID));
DROP USER 'sensor'@'localhost';
DROP USER 'analyzer'@'localhost';
FLUSH PRIVILEGES;
CREATE USER 'sensor'@'localhost' IDENTIFIED BY 'se123';
CREATE USER 'analyzer'@'localhost' IDENTIFIED BY '456ana';
FLUSH PRIVILEGES;
DROP DATABASE SensorList2;
CREATE DATABASE SensorList2;
USE SensorList2;
--DROP TABLE Sensor;
--DROP TABLE Users;
--DROP TABLE Mode;
CREATE TABLE Sensor (SensorID INT NOT NULL AUTO_INCREMENT, SensorType CHAR(15), SensorState CHAR(15), SensorColor CHAR, PRIMARY KEY (SensorID),INDEX(SensorID));
--SensorType: DHT11, DHT22... / SensorState: old, new / SensorNumber: 1, 2, 3 (sollen entsprechend markiert sein)
CREATE TABLE SensorVals (ValueID INT NOT NULL AUTO_INCREMENT, CurrentDateTime DATETIME DEFAULT CURRENT_TIMESTAMP, Temperature FLOAT(9,7), Humidity FLOAT(9,7), Sensor_ID INT NOT NULL, PRIMARY KEY (ValueID), FOREIGN KEY (Sensor_ID) REFERENCES Sensor(SensorID)) ENGINE=InnoDB;
INSERT INTO Sensor (SensorType,SensorState,SensorColor) VALUES ("BME280","old",'g');
INSERT INTO Sensor (SensorType,SensorState,SensorColor) VALUES ("BME280","old",'b');
INSERT INTO Sensor (SensorType,SensorState,SensorColor) VALUES ("SHT31","new",'g');
INSERT INTO Sensor (SensorType,SensorState,SensorColor) VALUES ("SHT31","new",'b');
INSERT INTO Sensor (SensorType,SensorState,SensorColor) VALUES ("HTU21D","new",'g');
INSERT INTO Sensor (SensorType,SensorState,SensorColor) VALUES ("HTU21D","new",'b');
INSERT INTO Sensor (SensorType,SensorState,SensorColor) VALUES ("AHT25","new",'g');
INSERT INTO Sensor (SensorType,SensorState,SensorColor) VALUES ("AHT25","new",'b');
INSERT INTO Sensor (SensorType,SensorState,SensorColor) VALUES ("DHT20","new",'g');
INSERT INTO Sensor (SensorType,SensorState,SensorColor) VALUES ("DHT20","new",'b');
INSERT INTO Sensor (SensorType,SensorState,SensorColor) VALUES ("DHT22","old",'g');
INSERT INTO Sensor (SensorType,SensorState,SensorColor) VALUES ("DHT22","old",'b');
INSERT INTO Sensor (SensorType,SensorState,SensorColor) VALUES ("DHT22","new",'o');
INSERT INTO Sensor (SensorType,SensorState,SensorColor) VALUES ("DHT22","new",'p');
GRANT USAGE ON *.* TO 'analyzer'@'localhost';
GRANT USAGE ON *.* TO 'sensor'@'localhost';
GRANT SELECT ON SensorList2.Sensor TO 'analyzer'@'localhost';
GRANT SELECT ON SensorList2.Sensor TO 'sensor'@'localhost';
GRANT SELECT ON SensorList2.SensorVals TO 'analyzer'@'localhost';
GRANT INSERT ON SensorList2.SensorVals TO 'sensor'@'localhost';
FLUSH PRIVILEGES;